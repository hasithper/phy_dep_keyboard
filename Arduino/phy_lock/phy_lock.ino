#include <SPI.h>
#include <SD.h>


Sd2Card card;
SdVolume volume;
SdFile root;



#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3);
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F, 16, 2);

#include <DS3231.h>
DS3231  rtc(SDA, SCL);

#define snd 9
#define relay 10
void setup() {
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  Serial.println("Setup");

  init_lcd();


  init_sd();
  init_fpsnsr();
delay(100);
lcd.clear();

  init_pins();
   rtc.begin();


}
int id = 0;
void loop() {
  
  //print current time

  
  id = getFingerprintIDez();
  //Serial.println(id);
  if (id > 0) {
    //finger correct

    lcd.clear();
    lcd.print("Finger print ok");
    lcd.setCursor(0, 1);
    //find name
    String tmpname=sdread(id);
    Serial.println(String(id)+" - "+tmpname);
    wrt_sd(tmpname,id);
    crrctsnd();
    shut_door();
  
    

  } else if (id == -3 | id==0) {
    //invalid id
    lcd.clear();
    lcd.print("Error! Pls Retry");
    if(id==0){
    lcd.setCursor(0, 0);
    lcd.print("Hi Dev :P No access\n");
    admin_snd();
      }
    
    
    digitalWrite(snd, HIGH);
    delay(100);
    digitalWrite(snd, LOW);

  }
  delay(500);
  lcd.clear();
  lcd.print("Date:");
  lcd.setCursor(6,0);
  lcd.print(rtc.getDateStr());
  lcd.setCursor(0,1);
   lcd.print("Time:");
  lcd.setCursor(6,1);
  lcd.print(rtc.getTimeStr());
}
