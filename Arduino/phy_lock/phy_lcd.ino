void init_lcd() {
  lcd.begin();
  lcd.backlight();
  lcd.print("Initialize");
}

void lcd_err(String a, int err) {
  lcd.clear();
  lcd.print("Initialize");
  lcd.setCursor(0, 1);
  lcd.print(a);
  lcd.setCursor(12, 1);
  if (err > 0)
    lcd.print("- ok");
  else
    lcd.print("- Er");
}


